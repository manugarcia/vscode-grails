
import { ExtensionContext, languages} from 'vscode';
import GroovyDocumentSymbolProvider from './groovy_document_symbol_provider'

export function activate(context: ExtensionContext) {
  console.log("groovy symbols active v 0.0.1");
  var selector = {
    language: 'groovy',
    scheme: 'file'
  };
  context.subscriptions.push(languages.registerDocumentSymbolProvider( selector, new GroovyDocumentSymbolProvider));
}

export function deactivate() {}